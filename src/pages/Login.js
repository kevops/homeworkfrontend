import React from 'react';
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import '../App.css';


function Login() {
    let navigate = useNavigate();
  const { register, handleSubmit, watch, formState: { errors } } = useForm();
  //const onSubmit = data => console.log(data);
    const onSubmit = data => {
        navigate("/main");
    }

  return (
    <div className="back fondo">
      <header >
        
        <h1>
          Login
        </h1>
       
      </header>
      <div className='formBlock'>
        <form onSubmit={handleSubmit(onSubmit)} className='form'>
          <label>e-mail: </label>
          <input type="text" placeholder="email address" {...register("email")} autoComplete='false'/>
          <label>Password: </label>
          <input type="password" placeholder="Password" {...register("password")} autoComplete='false' />
          <input type="submit" value="Login" />
        </form>
      </div>
    </div>
  );
}

export default Login;

