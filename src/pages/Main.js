import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import "../App.css";
import axios from "axios";




export default function Main() {
  let navigate = useNavigate();
  const [after, setAfter] = useState(false);
  const [offset, setOffset] = useState(10)
  const [pokeData, setPokeData] = useState([]);
  const  getPokemons = () =>  {
    try {
      axios.get('https://pokeapi.co/api/v2/pokemon?limit=10&offset=0')
      .then(resp => {
          for(let i =0; i < resp.data.results.length; i++){
            axios.get(resp.data.results[i].url)
            .then(result =>{
                setPokeData(pokeData => [...pokeData, result.data])
            })
          }
          
      })
    } catch (error) {
      console.error(error);
    }
  }
  const getPokemonsAfter = () =>  { 
    try {
        axios.get('https://pokeapi.co/api/v2/pokemon?limit=10&offset='+offset)
        .then(resp => {
            for(let i = 0; i < resp.data.results.length; i++){
                axios.get(resp.data.results[i].url)
                .then(result =>{
                    setPokeData(pokeData => [...pokeData, result.data])
                })
            } 
            setOffset(offset => offset + 10);
        })
    } catch (error) {
        console.error(error);
    }
    }
   


  useEffect(getPokemons,[]);


  return (
    <div>
      <h1>Pokemons</h1>
      <div className="responsiveBlock">
        
        {pokeData.map((pokemon) => (
          <div key={pokemon.id} className="pokeBlock">
            <h2>{pokemon.name}</h2>
            <img src={pokemon.sprites.front_default} alt={pokemon.name} />
          </div>
        ))}
      </div>

      <div>
        <button>Next</button>
        <input type={'button'} onClick={() => getPokemonsAfter() } value="Next" />
        {after ? <button>Before</button> : <></>}
      </div>
     
      <input type={'button'} onClick={() => {navigate('/')} } value="LogOut" />
    </div>
  );
}


/*
axios
      .get("https://pokeapi.co/api/v2/pokemon/")
      .then((res) => {
        return res.data.results;
      })
      .then((results) => {
        return Promise.all(results.map((res) => axios.get(res.url)));
      })
      .then((results) => {
        setPokeData(results.map((res) => res.data));
        console.log("Desde useEffect",pokeData);
      });

*/